# Changelog

## 2.0.0 (2023-12-29)

** Consider this an alpha release! **

My build process doesn't handle versions numbers like `2.0.0-alpha1`
correctly so I'm just calling this release `2.0.0`, but it is an initial
release after significant changes and re-architecture. If you're currently
running an earlier version that works well for you and you do not want to
act as an alpha-tester then do not install this release.

Note: there's no feedback for errors in the UI yet, so if you run into
anything you'll need to run `sailfish-qml harbour-coinfest` from the
terminal / CLI.

If you're upgrading from an earlier release you should remove the Coinfest
config and data files and start from scratch. The following command should
help locating them:

- `find ~ -name \*harbour-coinfest\*`

I'm kinda winging this because I think I'm the only person using this app
and also this new version shouldn't read those files anyway.

### Changes

- The config file format has changed, you cannot use the old config
- The tickers file format has changed, you cannot use the old tickers file
- This app now only uses QML and JavaScript so should work on all Sailfish
  OS architectures.

## 1.1 (2021-05-13)

### Added

- `aarch64` and `i486` packages.

### Changed

- Bring source in sync with [razcampagne](https://openrepos.net/user/856/programs)'s ECL and EQL5 repository.

## 1.0 (2021-03-03)

### Changed

- Use `harbour-coinfest` instead of `coinfest` as name on the system
  side of things.

### Fixed

- Use requested Kraken asset pairs instead of requesting them again.

### Removed

- Do not compile in Kraken asset pairs.  It leads to more troubles than
  it is worth.

## 0.11 (2021-03-02)

### Added

- Reset poll time on manual refresh.
- Naive migration from old to new poll time format.

### Changed

- Make poll times similar to Jolla apps.
    - But no 5 minute poll time! (too short)
- Replace BusyLabel with custom widget.

### Fixed

- Use BackgroundJob instead of Timer.

## 0.10 (2021-02-02)

### Changed

- Increase delay between API calls to 0.1s.

### Fixed

- Better handling of network errors.
- Show default setting when no config file exists.

## 0.09 (2021-01-29)

First released version.

## Resources

- https://keepachangelog.com/

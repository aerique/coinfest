import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.KeepAlive 1.2

Page {
    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: column.height

        VerticalScrollDecorator {}

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader { title: "Settings" }

            ComboBox {
                id: tickerRefresh
                objectName: "tickerRefresh"
                width: parent.width
                description: "between refreshing all tickers"
                currentIndex: switch (g_refresh) {
                    case BackgroundJob.FifteenMinutes : return 0;
                    case BackgroundJob.ThirtyMinutes  : return 1;
                    case BackgroundJob.OneHour        : return 2;
                    case BackgroundJob.FourHours      : return 3;
                    case BackgroundJob.TwelveHours    : return 4;
                    default: return 2;
                }
                menu: ContextMenu {
                    MenuItem { text: "15 minutes" }  // 0
                    MenuItem { text: "30 minutes" }  // 1
                    MenuItem { text: "1 hour"     }  // 2
                    MenuItem { text: "4 hours"    }  // 3
                    MenuItem { text: "12 hours"   }  // 4
                    onClicked: function() {
                        switch (tickerRefresh.currentIndex) {
                            case 0: g_refresh = BackgroundJob.FifteenMinutes;
                                    break;
                            case 1: g_refresh = BackgroundJob.ThirtyMinutes;
                                    break;
                            case 2: g_refresh = BackgroundJob.OneHour;
                                    break;
                            case 3: g_refresh = BackgroundJob.FourHours;
                                    break;
                            case 4: g_refresh = BackgroundJob.TwelveHours;
                                    break;
                            default: g_refresh = BackgroundJob.OneHour;
                        }
                        saveSettings();
                        tickerRefreshTimer.finished();
                    }
                }
            }

            SectionHeader { text: "About" }

            Label {
                width: parent.width - 2 * Theme.paddingLarge
                x: Theme.paddingLarge
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                textFormat: Text.RichText
                onLinkActivated: Qt.openUrlExternally(link)

                text:
                "<style>a:link { color: " + Theme.primaryColor + " }</style>" +

                "<p>Coinfest is an app to show tickers from cryptocurrency exchanges.  It was written by Erik Winkels <a href='mailto:aerique@xs4all.nl'>aerique@xs4all.nl</a>.</p>" +

                "<p>The source code is available at: <a href='https://codeberg.org/aerique/coinfest'>https://codeberg.org/aerique/coinfest</a></p>" +

                "<p>App icon made by <a href='https://www.flaticon.com/authors/dmitri13'>dmitri13</a> from <a href='https://www.flaticon.com/'>www.flaticon.com</a>.</p>"
            }
        }
    }

    // Functions

    function saveSettings () {
        log('writing config to ' + g_config_file);
        writeToFile(g_config_file, { 'refresh':  g_refresh });
    }
}

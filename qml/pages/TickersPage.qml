import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    SilicaListView {
        id: slv
        anchors.fill: parent
        model: tickers.model

        header: PageHeader { title: "Tickers" }

        delegate: ListItem {
            id: listEntry

            Label {
                id: cpLabel
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                }
                width: (parent.width - (2 * Theme.horizontalPageMargin)) * 0.4
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                text: model.ticker
            }

            Label {
                anchors {
                    top: cpLabel.bottom
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                }
                width: (parent.width - (2 * Theme.horizontalPageMargin)) * 0.4
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                font.pixelSize: Theme.fontSizeExtraSmall
                text: model.exchange
            }

            Label {
                id: priceLabel
                anchors {
                    left: cpLabel.right
                    leftMargin: Theme.paddingSmall
                }
                width: (parent.width - (2 * Theme.horizontalPageMargin)) * 0.3
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                //font.pixelSize: Theme.fontSizeLarge
                text: model.price
            }

            Label {
                anchors {
                    top: priceLabel.bottom
                    left: cpLabel.right
                    leftMargin: Theme.paddingSmall
                }
                width: (parent.width - (2 * Theme.horizontalPageMargin)) * 0.3
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                font.pixelSize: Theme.fontSizeExtraSmall
                text: model.time
            }

            Label {
                id: prevPriceLabel
                anchors {
                    left: priceLabel.right
                    leftMargin: Theme.paddingSmall
                }
                width: (parent.width - (2 * Theme.horizontalPageMargin)) * 0.3
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                color: Theme.highlightColor
                text: model.prev_price
            }

            Label {
                anchors {
                    top: prevPriceLabel.bottom
                    left: priceLabel.right
                    leftMargin: Theme.paddingSmall
                }
                width: (parent.width - (2 * Theme.horizontalPageMargin)) * 0.3
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                font.pixelSize: Theme.fontSizeExtraSmall
                color: Theme.highlightColor
                text: model.prev_time
            }

            menu: ContextMenu {
                MenuItem {
                    text: "Delete"
                    // XXX use remorseAction in Pusfofefe?
                    onClicked: listEntry.remorseAction("Deleting ticker",
                        function() {
                            log('deleting ticker ' + model.ticker);
                            tickers.model.remove(index);
                            writeTickers();
                        })
                }
            }
        }

        PullDownMenu {
            MenuItem {
                text: "Settings"
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }

            MenuItem {
                text: "Add Ticker"
                onClicked: pageStack.push(Qt.resolvedUrl("AddTickerDialog.qml"))
            }

            MenuItem {
                text: "Refresh"
                onClicked: function() {
                    log('triggering ticker refresh')
                    tickerRefreshTimer.begin()
                    //setOverviewModelTimer.running = true
                }
            }
        }

        VerticalScrollDecorator {}

        // This didn't work when directly positioned below `delegate:` ?!
        ViewPlaceholder {
            enabled: slv.count == 0
            text: "No tickers"
            hintText: "Pull down to add a ticker"
        }
    }
}

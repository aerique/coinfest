import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    // This means the top Kraken ticker cannot be accepted right away,
    // but: meh.
    //property bool tickerUsed: false

    Column {
        id: column
        spacing: Theme.paddingLarge
        width: parent.width

        // FIXME Why does this give a warning once here but not in Pusfofefe?!
        DialogHeader {}

        ComboBox {
            id: exchange
            width: parent.width
            description: 'exchange'
            menu: ContextMenu {
                      MenuItem {
                          text: 'Kraken'
                          onClicked: retrieveAssetPairs()
                      }
                  }
        }

        // Needs a search box.
        ComboBox {
            id: ticker
            width: parent.width
            description: 'ticker'
            menu: ContextMenu {
                Repeater {
                    model: assetPairs.model
                    MenuItem { text: model.wsname }
                }
            }
        }

        Label {
            width: parent.width - 2 * Theme.horizontalPageMargin
            x: Theme.horizontalPageMargin
            font.pixelSize: Theme.fontSizeSmall
            color: Theme.highlightColor
            wrapMode: Text.WordWrap
            text: '<br>Selecting an exchange will refresh its tickers.'
        }
    }

    //canAccept: tickerUsed == true

    onAccepted: {
        // Ok, this is pretty hopeless. Is there a better way to get this
        // from the ListModel?
        var param_name = '';
        for (var i = 0 ; i < assetPairs.model.count ; i++) {
            var item = assetPairs.model.get(i);
            if (item.wsname && item.wsname.indexOf(ticker.value) != -1) {
                log(item);
                if (item.wsname.indexOf('XBT') == 0) {
                    param_name = 'BTC' + item.wsname.substr(3);
                    ticker.value = param_name;
                } else if (item.wsname.indexOf('XBT') > 0) {
                    var i = item.wsname.indexOf('XBT');
                    param_name = item.wsname.substr(0, i) + 'BTC';
                    ticker.value = param_name;
                } else {
                    param_name = item.wsname;
                }
                break;
            }
        }
        if (param_name.length == 0) {
            log('cannot find param name for ' + ticker.value);
            // TODO warn in app we cannot use this asset pair
            return;
        }
        // `zz-stub` is temporary because for some reason the last item
        // gets duplicated (on append)
        tickers.model.append({ exchange   : exchange.value,
                               ticker     : ticker.value,
                               param_name : param_name,
                               time       : '',
                               price      : '',
                               prev_time  : '',
                               prev_price : '',
                               zzstub     : ''  });
        log(tickers.model.get(tickers.model.count - 1));
        tickerRefreshTimer.begin();
     }
}

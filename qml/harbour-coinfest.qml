import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.KeepAlive 1.2
import Nemo.Notifications 1.0

import 'components'

import 'pages/' as Pages

ApplicationWindow
{
    id: appwin

    property string g_app: 'Coinfest'
    property string g_version: '2.0.0'

    property bool g_verbose: true  // disable on final release

    // This one is stored in `config.json`.
    property int g_refresh: BackgroundJob.OneHour

    property string g_asset_pairs: '{}'
    property string g_tickers: '[]'

    property string g_config_file: StandardPaths.data + '/config.json'
    property string g_tickers_file: StandardPaths.cache + '/tickers.json'

    initialPage: Component { Pages.TickersPage {} }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    Component.onCompleted: {
        log('appwin completed');
        var ap = retrieveAssetPairs();
        g_asset_pairs = JSON.stringify(ap.result);
        log('reading config from ' + g_config_file);
        try {
            loadSettings();
        } catch (e) {
            log('no config found (' + e + ')');
        }
        log('reading tickers from ' + g_tickers_file);
        try {
            loadTickers();
            tickerRefreshTimer.begin();
        } catch (e) {
            log('no tickers found (' + e + ')');
        }
    }

    Component.onDestruction: log('appwin destroyed')

    onActiveFocusChanged: log('focus changed')

    JSONListModel {
        id: assetPairs
        json: g_asset_pairs
        query: '$.*'
    }

    JSONListModel {
        id: tickers
        json: g_tickers
        query: '$.[*]'
    }

    // XXX use BackgroundJob in Pusfofefe?
    BackgroundJob {
        id: tickerRefreshTimer
        frequency: g_refresh
        enabled: true
        onTriggered: function() {
            var json = retrieveTickers();
            var date = new Date();
            var hms = pad(date.getHours())   + ':' +
                      pad(date.getMinutes()) + ':' +
                      pad(date.getSeconds());
            for (var i = 0 ; i < tickers.model.count ; i++) {
                var ticker = tickers.model.get(i);
                tickers.model.setProperty(i, "prev_price", ticker.price);
                tickers.model.setProperty(i, "prev_time", ticker.time);
                var price = json.result[ticker.param_name]['p'][1];
                tickers.model.setProperty(i, "price", price);
                tickers.model.setProperty(i, "time", hms);
            }
            try {
                writeTickers();
            } catch (e) {
                log('could not write tickers (' + e + ')');
            }
            tickerRefreshTimer.finished();
        }
    }

    // Functions

    function log (str_or_obj) {
        if (g_verbose) { console.log(JSON.stringify(str_or_obj)); }
    }

    function pad (number) {
        if (number < 10) {
            return '0' + number;
        } else {
            return number;
        }
    }

    function retrieveAssetPairs () {
        log('retrieving asset pairs');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.kraken.com/0/public/AssetPairs', false);
        xhr.setRequestHeader('User-Agent', g_app + ' ' + g_version);
        xhr.send();
        try {
            var json = JSON.parse(xhr.responseText);
            if (json.error.length == 0) {
                return { 'success': true, 'result': json.result };
            } else {
                log(xhr.responseText);
                return { 'success': false, 'msg': xhr.responseText };
            }
        } catch (e) {
            log(e);
            return { 'success': false, 'msg': e };
        }
        return { 'success': false, 'msg': 'unknown (fell through)' };
    }

    function retrieveTickers () {
        log('retrieving tickers');
        var pair = '';
        for (var i = 0 ; i < tickers.model.count ; i++) {
            var ticker = tickers.model.get(i);
            pair += ticker.param_name;
            if (i < tickers.model.count - 1) {
                pair += ',';
            }
        }
        if (pair.length == 0) {
            log('no tickers to retrieve');
            return;
        }
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.kraken.com/0/public/Ticker?pair=' + pair,
                 false);
        xhr.setRequestHeader('User-Agent', g_app + ' ' + g_version);
        xhr.send();
        try {
            var json = JSON.parse(xhr.responseText);
            if (json.error.length == 0) {
                return { 'success': true, 'result': json.result };
            } else {
                log(xhr.responseText);
                return { 'success': false, 'msg': xhr.responseText };
            }
        } catch (e) {
            log(e);
            return { 'success': false, 'msg': e };
        }
        return { 'success': false, 'msg': 'unknown (fell through)' };
    }

    function loadSettings () {
        var config = JSON.parse(readFromFile(g_config_file));
        g_refresh  = config.refresh;
    }

    function loadTickers () {
        g_tickers = readFromFile(g_tickers_file);
    }

    function readFromFile (file) {
        var req = new XMLHttpRequest();
        req.open('GET', 'file://' + file, false);
        req.send();
        return req.responseText;
    }

    function writeToFile (file, str_or_obj) {
        var req = new XMLHttpRequest();
        req.open('PUT', 'file://' + file, false);
        req.send(JSON.stringify(str_or_obj) + '\n');
        return req.status;
    }

    function writeTickers () {
        var items = []
        for (var i = 0 ; i < tickers.model.count ; i++) {
            items.push(tickers.model.get(i));
        }
        log('writing tickers to ' + g_tickers_file);
        writeToFile(g_tickers_file, items);
    }
}

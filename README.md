# Coinfest

- **status**: working on a new version
- [Changelog](CHANGELOG.md)

A cryptocurrency tickers app for Sailfish OS.

The canonical home page of this project is https://codeberg.org/aerique/coinfest

(This project is also pushed to GitLab and Sourcehut but those sites are
not monitored for support.)

## Known Issues

- Kraken is the only supported exchange at the moment (I don't need
  anything else).

## Build

### Dependencies

- [SailfishOS Builds in Docker](https://codeberg.org/aerique/sfosbid)
  (SFOSBID)

### Instructions

The project was build using [SFOSBID](https://codeberg.org/aerique/sfosbid).

Refer to that project's README on how to get it running.

The actual build steps for Coinfest are:

Initial steps:

- copy `Dockerfile-sfosbid-project` from SFOSBID to the project root of
  Coinfest.
- `/path/to/sfosbid-git/bin/project-build.sh`

Steps for every dev session:

- `/path/to/sfosbid-git/bin/project-run.sh`
    - keep this terminal running
- `/path/to/sfosbid-git/bin/project-sync.sh`
    - run this in another terminal window
    - this will keep running unless interrupted by CTRL-C
    - it will rerun the build whenever one of the files listen in `git
      ls-files` is changed

## To Do

### High Priority

- [ ] ...

### Normal Priority

- [ ] figure out why the last JSON item in the ticker object gets doubled
      on append
- [ ] figure out errors when running from CLI for `DialogHeader`

### Low Priority

- [ ] graphs on ticker details page

## Attributions

App icon by [dmitri13](https://www.flaticon.com/authors/dmitri13) from
[www.flaticon.com](https://www.flaticon.com/).
